/*
 * matrixLed.cpp
 *
 *  Created on: 14 mars 2019
 *      Author: lsaintgermain
 */

#include <matrixLed.h>

namespace sixtron {

// Class WS2812B_LED
WS2812B_LED::WS2812B_LED()
{
	m_red = 0;
	m_green = 0;
	m_blue = 0;
	m_intensity = 0;
	m_position = 0;
	m_matrix = NULL;
}

WS2812B_LED::WS2812B_LED(WS2812B *matrix): WS2812B_LED()
{
	m_matrix = matrix;
}

WS2812B_LED::WS2812B_LED(WS2812B *matrix, int position): WS2812B_LED(matrix)
{
	set_position(position);
}

void WS2812B_LED::set_red(uint8_t red)
{
	m_red = red;
}

uint8_t WS2812B_LED::get_red()
{
	return m_red;
}

void WS2812B_LED::set_green(uint8_t green)
{
	m_green = green;
}

uint8_t WS2812B_LED::get_green()
{
	return m_green;
}

void WS2812B_LED::set_blue(uint8_t blue)
{
	m_blue = blue;
}

uint8_t WS2812B_LED::get_blue()
{
	return m_blue;
}

void WS2812B_LED::set_intensity(uint8_t intensity)
{
	if (intensity > 100) m_intensity = 255;
	else m_intensity = intensity*2.5;
}

uint8_t WS2812B_LED::get_intensity()
{
	return m_intensity;
}

void WS2812B_LED::set_position(int position)
{
	m_position = position;
}

int WS2812B_LED::get_position()
{
	return m_position;
}

WS2812B	*WS2812B_LED::get_matrix()
{
	return m_matrix;
}

void WS2812B_LED::set_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t intensity)
{
	set_red(red);
	set_green(green);
	set_blue(blue);
	set_intensity(intensity);
}

void WS2812B_LED::set_matrix_led_color()
{
	uint8_t Intensity = get_intensity();
	m_matrix->set_led_color(get_position(), (get_red()*Intensity)/255, (get_green()*Intensity)/255, (get_blue()*Intensity)/255);
}

void WS2812B_LED::clear_matrix_led()
{
	m_matrix->set_led_color(get_position(), 0, 0, 0);
}

void WS2812B_LED::update_LED()
{
	swo.printf("BASE WORKING!\n");
}

void WS2812B_LED::clear_matrix(WS2812B *matrix)
{
	for (int i = 0; i < LED_NUMBER; i++) {
		matrix->set_led_color(i, 0, 0, 0);
	}
}

void WS2812B_LED::clear_color()
{
	set_red(0);
	set_green(0);
	set_blue(0);
	set_intensity(0);
}

WS2812B_LED::~WS2812B_LED()
{
	// TODO Auto-generated destructor stub
}

// Class VERTICAL_LED
VERTICAL_LED::VERTICAL_LED(): WS2812B_LED()
{
	m_minPos = 0;
	m_maxPos = 0;
}

VERTICAL_LED::VERTICAL_LED(WS2812B *matrix, int min, int max): WS2812B_LED(matrix)
{
	m_minPos = min;
	m_maxPos = max;
}

void VERTICAL_LED::set_minPosition(int position)
{
	m_minPos = position;
}

int VERTICAL_LED::get_minPosition()
{
	return m_minPos;
}

void VERTICAL_LED::set_maxPosition(int position)
{
	m_maxPos = position;
}

int VERTICAL_LED::get_maxPosition()
{
	return m_maxPos;
}

int VERTICAL_LED::get_height()
{
	return fmin(m_maxPos-m_minPos+1, COLUMN_HEIGHT);
}

int VERTICAL_LED::get_length()
{
	return (get_maxPosition()/get_height()-get_minPosition()/get_height());
}

void VERTICAL_LED::update_LED(int position)
{
		clear_matrix_led();
		set_position(position);
		set_matrix_led_color();
}

VERTICAL_LED::~VERTICAL_LED()
{
	// TODO Auto-generated destructor stub
}

// Class WS2812B_LED
GRADIDENT_LED::GRADIDENT_LED(): VERTICAL_LED()
{
	m_x = true;
	m_y = true;
}

GRADIDENT_LED::GRADIDENT_LED(WS2812B *matrix, int position, int min, int max): VERTICAL_LED(matrix, min, max)
{
	set_position(position);
	m_x = true;
	m_y = true;
}

void GRADIDENT_LED::set_xGradient(bool enable)
{
	m_x = enable;
}

bool GRADIDENT_LED::get_xGradient()
{
	return m_x;
}

void GRADIDENT_LED::set_yGradient(bool enable)
{
	m_y = enable;
}

bool GRADIDENT_LED::get_yGradient()
{
	return m_y;
}

void GRADIDENT_LED::update_LED(double accel_x, double accel_y)
{
	int gravity_x = (int)(get_length()/2)+(int)(get_minPosition()/get_height())-(int)(accel_x/(2*MAX_GRAVITY/get_length()));
	int gravity_y = (int)(get_height()/2)+(int)(get_minPosition()%get_height())-(int)(accel_y/(2*MAX_GRAVITY/get_height()));
	int red_x = (1-fabs((int)(get_position()/get_height())-gravity_x)/(2*MAX_GRAVITY/get_length()))*255;
	int red_y = (1-fabs((int)(get_position()%get_height())-gravity_y)/(2*MAX_GRAVITY/get_height()))*255;
	int red = (((m_x)? red_x:red_y)+((m_y)? red_y:red_x))/2;
	set_red((uint8_t)(fmin(255, fmax(0, red))));
	set_green((uint8_t)(fmin(255, fmax(0, 255-red))));
	set_blue(0);
	set_matrix_led_color();
}

GRADIDENT_LED::~GRADIDENT_LED()
{
	// TODO Auto-generated destructor stub
}

TETRIS_LED::TETRIS_LED(): VERTICAL_LED(), m_tetrix{1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1}, m_checkLine{0, 0, 0, 0, 0, 0, 0, 0}
{
	m_pattern = 0;
	m_rotation = 0;
	m_state = INIT;
	m_score = 0;
	m_erased = false;

	m_nextPattern = 0;
	m_nextPosition = 0;
	trng_init(&trng);
}

TETRIS_LED::TETRIS_LED(WS2812B *matrix, int min, int max, int intensity): VERTICAL_LED(matrix, min, max), m_tetrix{1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1}, m_checkLine{0, 0, 0, 0, 0, 0, 0, 0}
{
	m_pattern = 0;
	m_rotation = 0;
	m_state = INIT;
	m_score = 0;
	m_erased = false;
	set_intensity(intensity);

	m_nextPattern = 0;
	m_nextPosition = 0;
	trng_init(&trng);
}

void TETRIS_LED::set_pattern(int pattern)
{
	m_pattern = pattern;
}

int	TETRIS_LED::get_pattern()
{
	return m_pattern;
}

void TETRIS_LED::set_rotation(int rotation)
{
	m_rotation = rotation;
}

int TETRIS_LED::get_rotation()
{
	return m_rotation;
}

void TETRIS_LED::set_checkLine(int position, bool value)
{
	m_checkLine[position] = value;
}

void TETRIS_LED::clear_checkLine()
{
	for (int i = 0; i < 8; i++) set_checkLine(i, 0);
}

void TETRIS_LED::set_score(int score)
{
	m_score = score;
}

int	TETRIS_LED::get_score()
{
	return m_score;
}

void TETRIS_LED::set_erased(bool value)
{
	m_erased = value;
}

bool TETRIS_LED::get_erased()
{
	return m_erased;
}

bool TETRIS_LED::begin_game()
{
	if (m_state == INIT) {
		m_nextPattern = random_value(NB_PATTERN, 0);
		m_nextPosition = random_value(NB_POSITION, (get_height()-NB_POSITION)/2);
		new_brick();
		for (int x = 0; x <= get_length()+(TETRIX_HEIGHT-get_height())/2; x++) {
			for (int y = 0; y < get_height(); y++) {
				set_tetrix_position(x*TETRIX_HEIGHT + y +(TETRIX_HEIGHT-get_height())/2, 0);
			}
		}
		set_score(0);
		m_state = CREATE;
		return true;
	} else {
		return false;
	}
}

STATE TETRIS_LED::get_state()
{
	return m_state;
};

void TETRIS_LED::update_LED(double accel_y, int rotation, bool movement_x)
{
	switch (m_state) {
	case CREATE:
		if (movement_x) {
			new_brick();
			if (check_positionRotation(get_position(), get_rotation())) m_state = MOVEMENT;
			else m_state = END_GAME;
		}
		break;
	case MOVEMENT:
		if (rotation != get_rotation())	update_rotation(rotation);
		update_positionY(accel_y);
		if (movement_x) {
			if (!update_positionX()) {
				if (check_line()) m_state = SCORING;
				else m_state = CREATE;
			}
		}
		break;
	case SCORING:
		update_score();
		m_state = GRAVITY;
		break;
	case GRAVITY:
		if (movement_x) {
			destroy_bricks();
			if (!check_line()) m_state = CREATE;
		}
		break;
	case END_GAME:
		swo.printf("GAME ENDED: Score = %d!\n", get_score());
		m_state = INIT;
		break;
	default:
		break;
	}
}

int TETRIS_LED::random_value(int modulo, int offset)
{
	size_t value = 1;
	trng_get_bytes(&trng, &trng_value, TRUE_RAND_DEPTH, &value);
	return (trng_value%modulo)+offset;
}

void TETRIS_LED::random_pattern()
{
	m_nextPattern = random_value(NB_PATTERN, 0);
	m_nextPosition = random_value(NB_POSITION, (get_height()-NB_POSITION)/2);
}

void TETRIS_LED::set_tetrix_position(int position, bool value)
{
	m_tetrix[position] = value;
}

bool TETRIS_LED::get_tetrix_position(int position)
{
	return m_tetrix[position];
}

void TETRIS_LED::new_brick()
{
	m_pattern = m_nextPattern;
	set_position(m_nextPosition);
	m_rotation = 0;
	random_pattern();
}

bool TETRIS_LED::check_positionRotation(int new_position, int new_rotation)
{
	int posX = new_position/get_height();
	int posY = new_position%get_height();
	int position = (posX+(TETRIX_HEIGHT-get_height())/2)*TETRIX_HEIGHT + posY +(TETRIX_HEIGHT-get_height())/2;
	for (int i = 0; i < BRICK_LENGTH; i++) {
		if ((TETRIS_PATTERN[m_pattern][READ_PATTERN[new_rotation][i]])&&(get_tetrix_position(position+CHECK_PATTERN[i]))) {
			return false;
		}
	}
	return true;
}

void TETRIS_LED::update_rotation(int new_rotation)
{
	if (check_positionRotation(get_position(), new_rotation)) {
		update_brickMovement(get_position(), new_rotation);
	}
}

void TETRIS_LED::update_positionY(double accel_y)
{
	int accel = (int)(get_height()/2)+(int)(get_minPosition()%get_height())-(int)(accel_y/(2*MAX_GRAVITY/(get_height()+1)));
	accel = fmax(0, fmin(get_height()-1, accel));
	int new_position = get_position();
	if (accel > new_position%get_height()) new_position = new_position+1;
	else if (accel < new_position%get_height()) new_position = new_position-1;
	if (check_positionRotation(new_position, get_rotation())) {
		update_brickMovement(new_position, get_rotation());
	} else update_brickMovement(get_position(), get_rotation());
}

bool TETRIS_LED::update_positionX()
{
	int new_position = get_position()+get_height();
	if (check_positionRotation(new_position, get_rotation())) {
		update_brickMovement(new_position, get_rotation());
		return true;
	} else {
		int posX = get_position()/get_height();
		int posY = get_position()%get_height();
		int position = (posX+(TETRIX_HEIGHT-get_height())/2)*TETRIX_HEIGHT + posY +(TETRIX_HEIGHT-get_height())/2;
		for (int i = 0; i < BRICK_LENGTH; i++) {
			if (TETRIS_PATTERN[m_pattern][READ_PATTERN[m_rotation][i]]) set_tetrix_position(position+CHECK_PATTERN[i], true);
		}
		update_brickMovement(get_position(), get_rotation(), true);
	}
	return false;
}

void TETRIS_LED::update_brickMovement(int new_position, int new_rotation, bool fixed)
{
	for (int i = 0; i < BRICK_LENGTH; i++) {
		if (TETRIS_PATTERN[m_pattern][READ_PATTERN[m_rotation][i]]) {
			// Clear Brick
			get_matrix()->set_led_color(get_position()+PRINT_PATTERN[i], 0, 0, 0);
		}
		if (TETRIS_PATTERN[m_pattern][READ_PATTERN[new_rotation][i]]) {
			// Display Brick
			if (!fixed) get_matrix()->set_led_color(new_position+PRINT_PATTERN[i], COLOR_PATTERN[m_pattern][0]*get_intensity()/255, COLOR_PATTERN[m_pattern][1]*get_intensity()/255, COLOR_PATTERN[m_pattern][2]*get_intensity()/255);
			else get_matrix()->set_led_color(new_position+PRINT_PATTERN[i], COLOR_FIXED_PATTERN[m_pattern][0]*get_intensity()/255, COLOR_FIXED_PATTERN[m_pattern][1]*get_intensity()/255, COLOR_FIXED_PATTERN[m_pattern][2]*get_intensity()/255);
		}
	}
	if (new_position != get_position()) set_position(new_position);
	if (new_rotation != get_rotation()) set_rotation(new_rotation);
}

bool TETRIS_LED::check_line()
{
	clear_checkLine();
	bool check = false;
	for (int i = (get_position()/get_height()); i <= get_length(); i++) {
		int indice = (i+(TETRIX_HEIGHT-get_height())/2)*TETRIX_HEIGHT;
		int count = 0;
		for (int j = 1; j <= get_height(); j++) {
			if (get_tetrix_position(indice+j)) count++;
			else break;
		}
		if (count == get_height()) {
			m_checkLine[i] = 1;
			check = true;
		}
	}
	return check;
}

void TETRIS_LED::update_score()
{
	int line = 0;
	for (int i = 0; i < (TETRIS_MAX/COLUMN_HEIGHT)+1; i++) {
		if (m_checkLine[i]) line++;
	}
	set_score(get_score() + line*line*(1+get_score()/10));
	swo.printf("Score: %d\n", get_score());
}

void TETRIS_LED::destroy_bricks()
{
	int line = 0;
	for (int i = 0; i < (TETRIS_MAX/COLUMN_HEIGHT)+1; i++) {
		if (m_checkLine[i]) line = i;
	}
	if (get_erased()) {
		set_erased(false);
		for (int i = line; i > 0; i--){
			for (int j = 0; j < get_height(); j++) {
				int pos_check = ((i+(TETRIX_HEIGHT-get_height())/2)*TETRIX_HEIGHT) + j+1;
				int pos_back_check = ((i-1+(TETRIX_HEIGHT-get_height())/2)*TETRIX_HEIGHT) + j+1;
				int pos_print = i*get_height()+j;
				int pos_back_print = (i-1)*get_height()+j;
				set_tetrix_position(pos_check, get_tetrix_position(pos_back_check));
				get_matrix()->set_led_color(pos_print, LEDScreen[pos_back_print][0], LEDScreen[pos_back_print][1], LEDScreen[pos_back_print][2]);
			}
		}
	} else {
		set_erased(true);
		for (int j = 0; j < get_height(); j++) {
			int pos_print = line*get_height()+j;
			get_matrix()->set_led_color(pos_print, 0, 0, 0);
		}
	}
//	update_gravity(line);
}

void TETRIS_LED::update_gravity(int end_line)
{

}

TETRIS_LED::~TETRIS_LED()
{
	// TODO Auto-generated destructor stub
}

} // Namespace sixtron
