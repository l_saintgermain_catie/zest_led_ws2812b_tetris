/*
 * matrixLed.h
 *
 *  Created on: 14 mars 2019
 *      Author: lsaintgermain
 */

#ifndef MATRIXLED_H_
#define MATRIXLED_H_

#include "define.h"
#include "ws2812.h"
#include "math.h"
#include "stdlib.h"
#include "trng_api.h"

namespace sixtron {

enum STATE {
	INIT,
	CREATE,
	MOVEMENT,
	SCORING,
	GRAVITY,
	END_GAME
};

static trng_t trng;
static uint8_t trng_value;

class WS2812B_LED {
public:
	WS2812B_LED();
	WS2812B_LED(WS2812B *matrix);
	WS2812B_LED(WS2812B *matrix, int position);

	void	set_red(uint8_t red);
	uint8_t	get_red();
	void	set_green(uint8_t green);
	uint8_t	get_green();
	void	set_blue(uint8_t blue);
	uint8_t	get_blue();
	void	set_intensity(uint8_t intensity);
	uint8_t	get_intensity();
	void	set_position(int position);
	int		get_position();
	WS2812B	*get_matrix();

	void	set_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t intensity);
	void	clear_color();
	void 	set_matrix_led_color();
	void	clear_matrix_led();
	virtual void update_LED();

	static	void clear_matrix(WS2812B *matrix);

	virtual	~WS2812B_LED();

private:
	uint8_t	m_red;
	uint8_t	m_green;
	uint8_t	m_blue;
	uint8_t	m_intensity;
	int		m_position;

	WS2812B	*m_matrix;
};

class VERTICAL_LED: public WS2812B_LED {
public:
	VERTICAL_LED();
	VERTICAL_LED(WS2812B *matrix, int min = 0, int max = 0);

	void	set_minPosition(int position);
	int		get_minPosition();
	void	set_maxPosition(int position);
	int		get_maxPosition();
	int		get_height();
	int		get_length();
	virtual void update_LED(int position);

	virtual ~VERTICAL_LED();

private:
	int m_minPos;
	int	m_maxPos;
};

class GRADIDENT_LED: public VERTICAL_LED {
public:
	GRADIDENT_LED();
	GRADIDENT_LED(WS2812B *matrix, int position, int min = 0, int max = 0);

	void	set_xGradient(bool enable);
	bool	get_xGradient();
	void	set_yGradient(bool enable);
	bool	get_yGradient();
	virtual void update_LED(double accel_x, double accel_y);

	virtual ~GRADIDENT_LED();

private:
	bool	m_x;
	bool	m_y;
};

class TETRIS_LED: public VERTICAL_LED {
public:
	TETRIS_LED();
	TETRIS_LED(WS2812B *matrix, int min = 0, int max = 0, int intensity = 10);

	void	set_pattern(int pattern);
	int		get_pattern();
	void 	set_rotation(int rotation);
	int		get_rotation();
	void	set_checkLine(int position, bool value);
	void	clear_checkLine();
	void	set_score(int score);
	int		get_score();
	void	set_erased(bool value);
	bool	get_erased();
	bool	begin_game();
	STATE	get_state();
	virtual void update_LED(double accel_y, int rotation, bool movement_x = false);

	virtual ~TETRIS_LED();

private:
	int 	random_value(int modulo, int offset);
	void	random_pattern();
	void	set_tetrix_position(int position, bool value);
	bool	get_tetrix_position(int position);
	void	new_brick();
	bool	check_positionRotation(int new_position, int new_rotation);
	void	update_rotation(int new_rotation);
	void	update_positionY(double accel_y);
	bool	update_positionX();
	void	update_brickMovement(int new_position, int new_rotation, bool fixed = false);
	bool 	check_line();
	void	update_score();
	void	destroy_bricks();
	void	update_gravity(int end_line);

	int		m_pattern;
	int		m_rotation;
	STATE	m_state;
	bool	m_tetrix[70];
	bool	m_checkLine[(TETRIS_MAX/COLUMN_HEIGHT)+1];
	int		m_score;
	bool	m_erased;

	int		m_nextPattern;
	int		m_nextPosition;

};

} // Namespace sixtron

#endif /* MATRIXLED_H_ */
