/*
 * define.h
 *
 *  Created on: 25 mars 2019
 *      Author: lsaintgermain
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#include "mbed.h"
#include "swo.h"
#include "assert.h"

#define LOOP_PERIOD_MS 1000
#define MATRIX_PERIOD_MS 10
#define BLINK_PERIOD_MS 500

#define MAX_GRAVITY 7
#define LPF_POLE 2

#define TRUE_RAND_DEPTH 1
#define TETRIS_MIN 0
#define TETRIS_MAX 39
#define TETRIS_PERIOD_MS 20
#define COLUMN_HEIGHT 5
#define NB_PATTERN 5
#define NB_POSITION 3
#define NB_ROTATION 4
#define BRICK_LENGTH 9
#define TETRIX_HEIGHT 7
#define GRAVITY_CYCLE 40

static sixtron::SWO swo;

static double LPF_WEIGHT[LPF_POLE] = {0.3, 0.7};

static bool TETRIS_PATTERN[NB_PATTERN][BRICK_LENGTH] = {	{0, 0, 0,
															 1, 1, 1,
															 0, 0, 0},
															{0, 0, 1,
															 1, 1, 1,
															 0, 0, 0},
															{0, 0, 0,
															 1, 1, 1,
															 0, 0, 1},
															{0, 1, 0,
															 1, 1, 0,
															 0, 0, 0},
															{0, 1, 1,
															 0, 1, 1,
															 0, 0, 0}	};
static int READ_PATTERN[NB_ROTATION][BRICK_LENGTH] = {	{0, 1, 2 ,3 ,4 ,5 ,6 ,7, 8},
														{2, 5, 8 ,1 ,4 ,7 ,0 ,3, 6},
														{8, 7, 6 ,5 ,4 ,3 ,2 ,1, 0},
														{6, 3, 0 ,7 ,4 ,1 ,8 ,5, 2}};
static int PRINT_PATTERN[BRICK_LENGTH] = {-COLUMN_HEIGHT-1, -COLUMN_HEIGHT, -COLUMN_HEIGHT+1, -1, 0, 1, COLUMN_HEIGHT-1, COLUMN_HEIGHT, COLUMN_HEIGHT+1};
static int CHECK_PATTERN[BRICK_LENGTH] = {-TETRIX_HEIGHT-1, -TETRIX_HEIGHT, -TETRIX_HEIGHT+1, -1, 0, 1, TETRIX_HEIGHT-1, TETRIX_HEIGHT, TETRIX_HEIGHT+1};
static int COLOR_PATTERN[NB_PATTERN][3] = {	{255, 0, 0},
											{0, 255, 0},
											{0, 0, 255},
											{255, 255, 0},
											{255, 0, 255}};
static int COLOR_FIXED_PATTERN[NB_PATTERN][3] = {	{255, 16, 16},
													{16, 255, 16},
													{16, 16, 255},
													{255, 255, 16},
													{255, 16, 255}};

#endif /* DEFINE_H_ */
