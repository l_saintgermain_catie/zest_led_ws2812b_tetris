/*
 * Copyright (c) 2017, CATIE, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "define.h"

#include "matrixLed.h"
#include "bno055.h"

using namespace sixtron;

// Input - Output
static DigitalOut led(LED1);
static InterruptIn button(BUTTON1);
static I2C i2c(I2C_SDA, I2C_SCL);

// WS2812 Led Declaration
static WS2812B led_matrix;
static VERTICAL_LED vert_led(&led_matrix, 0, 4);
static GRADIDENT_LED grad_led[35];

// TETRIS Led Declaration
static TETRIS_LED tetris(&led_matrix, TETRIS_MIN, TETRIS_MAX, 10);
static int gravity_cycle = 0;
static int tetris_rotation = 0;

// BNO055 Declaration
static BNO055 bno(&i2c);
static bno055_gravity_t array_accel[LPF_POLE];
static bno055_gravity_t filtered_accel;
static bool use_fusion = true;

// Mode declaration
enum MODE {
	STANDBY,
	TETRIS,
	MODE2,
	MODE3,
	MODE4,
	END_MODES
};
MODE mode = STANDBY;

// Events
static EventQueue queue;

// Threads
Thread eThread;

// Variables


// Low Pass Filter
void LP_filter()
{
	filtered_accel.x = 0;
	filtered_accel.y = 0;
	filtered_accel.z = 0;
	for (int i = 0; i < LPF_POLE; i++) {
		filtered_accel.x += LPF_WEIGHT[i]*array_accel[i].x;
		filtered_accel.y += LPF_WEIGHT[i]*array_accel[i].y;
		filtered_accel.z += LPF_WEIGHT[i]*array_accel[i].z;
	}
//	printf("Accel: \t%f\t %f\t %f\nFilter: \t%f\t %f\t %f\n", array_accel[0].x, array_accel[0].y, array_accel[0].z, filtered_accel.x, filtered_accel.x, filtered_accel.x);
}

// Refresh accelerometer data
void refresh_accel()
{
	for (int i = 1; i < LPF_POLE; i++) {
		array_accel[i] = array_accel[i-1];
	}
	array_accel[0] = bno.gravity();
	LP_filter();
}

void update_matrix()
{
	if (mode == STANDBY) {
		for (int i = 0; i < 35; i++) {
			grad_led[i].update_LED(filtered_accel.x, filtered_accel.y);
		}
	} else if (mode == TETRIS) {
		tetris.update_LED(filtered_accel.y, tetris_rotation, (gravity_cycle == GRAVITY_CYCLE-1)? true:false);
		tetris_rotation = tetris.get_rotation();
		gravity_cycle = (gravity_cycle+1)%GRAVITY_CYCLE;
		if (tetris.get_state() == INIT) {
			tetris_rotation = 0;
			mode = static_cast<MODE>((mode+1)%END_MODES);
			WS2812B_LED::clear_matrix(&led_matrix);
			vert_led.update_LED(mode);
		}
	}
}

// Refresh the matrix led for display
void refresh_matrix()
{
	led_matrix.refresh_screen(0);
}

// Brief Configure sensors for fusion or non-fusion mode
void configure_sensors(bool fusion)
{
    if (fusion) {
	bno.set_operation_mode(BNO055::OperationMode::NDOF);
    }
    else {
	bno.set_operation_mode(BNO055::OperationMode::ACCONLY);

	bno.set_accelerometer_configuration(BNO055::AccelerometerSensorRange::_4G, BNO055::AccelerometerSensorBandWidth::_62Hz, BNO055::AccelerometerSensorOperationMode::Normal);
	}
}

// Toggle runningBlink on interruption
void button_interrupt()
{
	if (mode == STANDBY) {
		mode = static_cast<MODE>((mode+1)%END_MODES);
		WS2812B_LED::clear_matrix(&led_matrix);
		tetris.begin_game();
	} else if (mode == TETRIS) {
		tetris_rotation = (tetris_rotation+1)%NB_ROTATION;
	} else {
		mode = static_cast<MODE>((mode+1)%END_MODES);
		WS2812B_LED::clear_matrix(&led_matrix);
		if (mode != TETRIS) vert_led.update_LED(mode);
	}
//	queue.call(printf, "Mode changed! %d\n", mode);
}

// main() runs in its own thread in the OS
int main()
{
// Init
	// Asserts
	double value = 0;
	for (int i = 0; i < LPF_POLE; i++) value += LPF_WEIGHT[i];
	assert(value == 1);
    // Leds Init
	vert_led.set_color(0, 160, 255, 20);
	vert_led.update_LED(mode);
	for (int i = 0; i < 35; i++) {
		grad_led[i] = GRADIDENT_LED(&led_matrix, i+5, 5, 39);
		grad_led[i].set_intensity(10);
	}
	// Accel Init
	if (bno.initialize(BNO055::OperationMode::NDOF, true) != true) {
		printf("ERROR BNO055 not detected. Check your wiring and BNO I2C address\n");
		return 0;
	}
	else {
		queue.call(configure_sensors, use_fusion);
		printf("BNO055 configured in fusion mode!\n");
	}
	swo.printf("Initialisation Done!\n");

// Repeat
	button.rise(&button_interrupt);
	eThread.start(callback(&queue, &EventQueue::dispatch_forever));
	queue.call_every(2*MATRIX_PERIOD_MS, refresh_accel);
    queue.call_every(TETRIS_PERIOD_MS, update_matrix);
    queue.call_every(MATRIX_PERIOD_MS, refresh_matrix);
}
